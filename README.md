### Qmusic Zabbix Sender voor Zabbix V4.x
Library voor integratie Zabbix sender

### Basic usage example
Zet dit in je packge.json onder dependencies: 

```javascript
"zabbix-sender-v4": "git+http://git/qnl/zabbix-sender-v4"
```

en dan: **`npm install`**

```javascript
var ZabbixSenderV4 = require('zabbix-sender-v4')
var sender = new ZabbixSenderV4({host: 'zabbix-v4'})

sender.addItem('ligacontroller03', 'state','ik ben ok')

sender.send(function(err, res,items) {
    if (err) console.error(err)
    
    console.log(res,items)
});
```
